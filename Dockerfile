# Build
FROM node:latest AS builder

RUN git clone https://github.com/vuejs/vuex.git /vuex

WORKDIR /vuex

RUN npm install
RUN npm run docs:build

# Serve
FROM nginx:alpine

COPY --from=builder /vuex/docs/.vuepress/dist /usr/share/nginx/html
